WEBVTT

00:00:05.480 --> 00:00:07.105
Hi, thank you.

00:00:07.879 --> 00:00:11.293
I'm Nicolas Dandrimont and I will indeed
be talking to you about

00:00:11.293 --> 00:00:12.551
Software Heritage.

00:00:12.876 --> 00:00:15.232
I'm a software engineer for this project.

00:00:15.639 --> 00:00:17.712
I've been working on it for 3 years now.

00:00:18.485 --> 00:00:21.572
And we'll see what this thing is all about.

00:00:23.767 --> 00:00:38.808
[Mic not working]

00:00:39.174 --> 00:00:40.752
I guess the batteries are out.

00:00:49.949 --> 00:00:51.720
So, let's try that again.

00:00:52.050 --> 00:00:55.380
So, we all know, we've been doing
free software for a while,

00:00:55.616 --> 00:00:59.806
that software source code is something
special.

00:01:00.779 --> 00:01:02.031
Why is that?

00:01:02.731 --> 00:01:09.963
As Harold Abelson has said in SICP, his
textbook on programming,

00:01:09.963 --> 00:01:18.782
programs are meant to be read by people
and then incidentally for machines to execute.

00:01:20.213 --> 00:01:25.661
Basically, what software source code
provides us is a way inside

00:01:25.661 --> 00:01:28.547
the mind of the designer of the program.

00:01:29.309 --> 00:01:37.938
For instance, you can have,
you can get inside very crazy algorithms

00:01:37.938 --> 00:01:46.564
that can do very fast reverse square roots
for 3D, that kind of stuff

00:01:47.211 --> 00:01:49.524
Like in the Quake 2 source code.

00:01:49.860 --> 00:01:54.606
You can also get inside the algorithms
that are underpinning the internet,

00:01:54.606 --> 00:01:59.765
for instance seeing the net queue
algorithm in the Linux kernel.

00:02:03.631 --> 00:02:10.218
What we are building as the free software
community is the free software commons.

00:02:10.948 --> 00:02:18.629
Basically, the commons is all the cultural
and social and natural resources

00:02:18.629 --> 00:02:21.802
that we share and that everyone
has access to.

00:02:22.410 --> 00:02:25.744
More specifically, the software commons
is what we are building

00:02:25.744 --> 00:02:31.878
with software that is open and that is
available for all to use, to modify,

00:02:31.878 --> 00:02:34.887
to execute, to distribute.

00:02:37.252 --> 00:02:45.251
We know that those commons are a really
critical part of our commons.

00:02:46.306 --> 00:02:48.137
Who's taking care of it?

00:02:49.684 --> 00:02:51.800
The software is fragile.

00:02:51.800 --> 00:02:54.405
Like all digital information, you can lose
software.

00:02:55.625 --> 00:03:01.634
People can decide to shut down hosting
spaces because of business decisions.

00:03:02.939 --> 00:03:08.913
People can hack into software hosting
platforms and remove the code maliciously

00:03:08.913 --> 00:03:10.864
or just inadvertently.

00:03:12.978 --> 00:03:17.898
And, of course, for the obsolete stuff,
there's rot.

00:03:18.468 --> 00:03:24.773
If you don't care about the data, then
it rots and it decays and you lose it.

00:03:26.157 --> 00:03:31.238
So, where is the archive we go to
when something is lost,

00:03:31.238 --> 00:03:33.965
when GitLab goes away, when Github
goes away.

00:03:34.411 --> 00:03:35.708
Where do we go?

00:03:36.519 --> 00:03:40.989
Finally, there's one last thing that we
noticed, it's that

00:03:40.989 --> 00:03:48.581
there's a lot of teams that work on
research on software

00:03:48.581 --> 00:03:54.310
and there's no real big infrastructure
for research on code.

00:03:56.510 --> 00:04:02.129
There's tons of critical issues around
code: safety, security, verification, proofs.

00:04:03.583 --> 00:04:07.694
Nobody's doing this at a very large scale.

00:04:08.466 --> 00:04:12.244
If you want to see the stars, you go
the Atacama desert and

00:04:12.244 --> 00:04:13.830
you point a telescope at the sky.

00:04:14.477 --> 00:04:17.526
Where is the telescope for source code?

00:04:17.973 --> 00:04:20.983
That's what Software Heritage wants to be.

00:04:22.081 --> 00:04:27.651
What we do is we collect, we preserve
and we share all the software

00:04:27.651 --> 00:04:29.887
that is publicly available.

00:04:31.139 --> 00:04:35.852
Why do we do that? We do that to
preserve the past, to enhance the present

00:04:35.852 --> 00:04:37.848
and to prepare for the future.

00:04:39.715 --> 00:04:44.588
What we're building is a base infrastructure
that can be used

00:04:44.588 --> 00:04:50.359
for cultural heritage, for industry,
for research and for education purposes.

00:04:50.724 --> 00:04:53.120
How do we do it? We do it with an open
approach.

00:04:53.406 --> 00:04:56.613
Every single line of code that we write
is free software.

00:04:59.088 --> 00:05:04.653
We do it transparently, everything that
we do, we do it in the open,

00:05:04.653 --> 00:05:09.124
be that on a mailing list or on
our issue tracker.

00:05:09.858 --> 00:05:15.873
And we strive to do it for the very long
haul, so we do it with replication in mind

00:05:15.873 --> 00:05:21.806
so that no single entity has full control
over the data that we collect.

00:05:22.945 --> 00:05:27.335
And we do it in a non-profit fashion
so that we avoid

00:05:27.335 --> 00:05:32.786
business-driven decisions impacting
the project.

00:05:35.470 --> 00:05:38.683
So, what do we do concretely?

00:05:39.009 --> 00:05:42.951
We do archiving of version control systems.

00:05:43.276 --> 00:05:44.617
What does that mean?

00:05:45.755 --> 00:05:49.411
It means we archive file contents, so
source code, files.

00:05:49.411 --> 00:05:55.673
We archive revisions, which means all the
metadata of the history of the projects,

00:05:55.673 --> 00:06:03.148
we try to download it and we put it inside
a common data model that is

00:06:03.148 --> 00:06:06.968
shared across all the archive.

00:06:08.555 --> 00:06:13.590
We archive releases of the software,
releases that have been tagged

00:06:13.590 --> 00:06:18.339
in a version control system as well as
releases that we can find as tarballs

00:06:18.339 --> 00:06:23.945
